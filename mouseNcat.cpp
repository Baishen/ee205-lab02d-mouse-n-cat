///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author  Baishen Wang <baishen@hawaii.edu>
/// @date    20_Jan_2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>

// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) {

	//std::cout << "mouseNcat" << std::endl;

   int i = argc -1;
   
   //print evironment variables if there are no parameters
   if (i == 0){
       for (char **env = envp; *env != 0; env++){
         char *thisEnv = *env;
         std::cout<< thisEnv <<"\n";    
         //printf("%s\n", thisEnv);
         }      
         return 0;
   }
   //print arguements given in reverse order

   while (i > -1) {
      std::cout << argv[i] << "\n";
      i -= 1;
   }

   std::exit( EXIT_SUCCESS );
}
